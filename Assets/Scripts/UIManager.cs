﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Properties
    [SerializeField] private Text _scoreText;
    [SerializeField] private Sprite[] _lives;
    [SerializeField] private Image _liveImage;
    [SerializeField] private Text _gameOverText;
    [SerializeField] private Text _gameWinnerText;
    [SerializeField] private Text _restartText;
    [SerializeField] private Text _ammoText;
    [SerializeField] private Text _thrustlerText;
    [SerializeField] private bool _gameOver = false;
    private WaitForSeconds _waitForSecs;
    private bool _showGameText = false;

    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
                Debug.LogError("UIManager is null");

            return _instance;
        }
    }

    #endregion

    #region UnityMethods
    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        _scoreText.text = "0";
        _liveImage.sprite = _lives[3];
        _gameOverText.gameObject.SetActive(false);
        _restartText.gameObject.SetActive(false);
        _waitForSecs = new WaitForSeconds(1f);
    }

    private void Update()
    {
        if (_gameOver)
            StartCoroutine("GameOverRoutine");   
    }

    #endregion

    #region PublicMethods
    public void UpdateScore(int score)
    {
        _scoreText.text = score.ToString();
    }

    public void UpdateLives(int live)
    {
        if (live >= 0)
            _liveImage.sprite = _lives[live];

        if (live == 0)
        {
            GameManager.Instance.SetGameOver();
            StartCoroutine("GameOverRoutine");
        }
    }

    public void SetGameWinner()
    {
        GameManager.Instance.SetWinner();
        StartCoroutine("WinnerGameRoutine");
    }

    public void UpdateAmmo(int ammo, int max)
    {
        _ammoText.text =  ammo + "/" + max;
    }

    public void UpdateThrustler(float thrustler)
    {
        _thrustlerText.text = "Thrustlers: " + thrustler.ToString("0");
    }
    #endregion

    #region Coroutines
    IEnumerator GameOverRoutine()
    {
        while (true)
        {
            _showGameText = _showGameText == true ? false : true;
            _gameOverText.gameObject.SetActive(_showGameText);
            _restartText.gameObject.SetActive(true);
            yield return _waitForSecs;            
        }
    }

    IEnumerator WinnerGameRoutine()
    {
        while (true)
        {
            _showGameText = _showGameText == true ? false : true;
            _gameWinnerText.gameObject.SetActive(_showGameText);
            _restartText.gameObject.SetActive(true);
            yield return _waitForSecs;
        }
    }
    #endregion
}
