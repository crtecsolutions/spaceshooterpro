﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpaceShip : Enemy
{
    #region Properties    
    [SerializeField] private GameObject _shield;    
    [SerializeField] protected GameObject _laserBackPrefab;
    private List<GameObject> _powerups;
    private WaitForSeconds _firePowerupWaitForSecs;    
    private Vector3 _currentPos;
    protected bool _isShieldEnabled = false;
    private bool _isFiringPowerup = false;
    #endregion

    #region UnityMethods

    protected override void Start()
    {
        base.Start();
        _firePowerupWaitForSecs = new WaitForSeconds(0.5f);        
        _powerups = new List<GameObject>();
        _currentPos = transform.position;
    }


    void Update()
    {        
        //this validation is required because the enemy keeps fire after visually dead because the gameobject perse is destroy after 2.8f
        //also is required in order to avoid the enemy from being moved when explotes        
        if (!_isDeath)
        {            
            Move();
            Fire();
            if (IsPowerupInRange())
                StartCoroutine("FireAtPowerup");
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player)
                player.Damage();

            DestroyEnemy();
        }
        else if (other.tag == "Laser")
        {
            Destroy(other.gameObject);
            DestroyEnemy();

            if (_player)
                _player.ScorePoints(10);
        }
    }
    #endregion


    #region Coroutines
    IEnumerator FireAtPowerup()
    {
        if (!_isFiringPowerup)
        {
            _isFiringPowerup = true;
            GameObject enemyLaser = Instantiate(_weaponShotPrefab, transform.position, Quaternion.identity);
            EnemyLaser[] lasers = enemyLaser.GetComponentsInChildren<EnemyLaser>();

            foreach (EnemyLaser laser in lasers)
            {
                laser.SetEnemyLaser(1);
            }
            yield return _firePowerupWaitForSecs;
            _isFiringPowerup = false;
        }
    }
    #endregion

    #region PrivateMethods  
    protected override void Move()
    {
        _currentPos += Vector3.down * Time.deltaTime * _speed;
        transform.position = _currentPos + transform.right * Mathf.Sin(Time.time * frequency);
        if (_currentPos.y < -5.4f)
        {
            float randX = Random.Range(-9.6f, 9.6f);
            _currentPos = new Vector3(randX, 7, 0);
        }

    }
    protected override void Fire()
    {
        if (Time.time > _canFire)
        {
            _shootRate = Random.Range(3f, 7f);
            _canFire = Time.time + _shootRate;


            if (_player)
            {
                if (transform.position.y < _player.transform.position.y)
                {
                    GameObject enemyLaser = Instantiate(_laserBackPrefab, transform.position, Quaternion.identity);
                    EnemyLaser[] lasers = enemyLaser.GetComponentsInChildren<EnemyLaser>();
                    foreach (EnemyLaser laser in lasers)
                    {
                        laser.SetEnemyLaser(2);
                    }
                }
                else
                {
                    GameObject enemyLaser = Instantiate(_weaponShotPrefab, transform.position, Quaternion.identity);
                    EnemyLaser[] lasers = enemyLaser.GetComponentsInChildren<EnemyLaser>();
                    foreach (EnemyLaser laser in lasers)
                    {
                        laser.SetEnemyLaser(1);
                    }
                }
            }
        }
    }
    protected override void DestroyEnemy()
    {
        if (_isShieldEnabled)
        {
            _isShieldEnabled = false;
            _shield.SetActive(false);
            return;
        }

        if (_animator)
            _animator.SetTrigger("OnEnemyDeath");

        _speed = 0;
        _audioSource.Play();
        Destroy(_collider2d);
        _isDeath = true;
        Destroy(this.gameObject, 2.8f);
    }
    private bool IsPowerupInRange()
    {
        bool result = false;
        _powerups.Clear();
        _powerups = GameObject.FindGameObjectsWithTag("Powerup").ToList();

        if (_powerups.Count > 0)
        {
            foreach (var item in _powerups)
            {
                bool inXrange = item.transform.position.x >= transform.position.x - 0.7f && item.transform.position.x <= transform.position.x + 0.7f;
                bool inYRange = item.transform.position.y + 3f >= transform.position.y ? true: false;

                result = inXrange && inYRange ? true : false;
                break;
            }
        }

        return result;
    }
    #endregion

    public void ActivateShield()
    {
        _isShieldEnabled = true;
        _shield.SetActive(true);
    }

}
