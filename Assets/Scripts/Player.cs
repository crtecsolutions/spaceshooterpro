﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    #region properties
    [SerializeField] float _speed = 3.5f;
    [SerializeField] private int _lives = 3;
    [SerializeField] private int _maxAmmo = 15;
    [SerializeField] private GameObject _laserPrefab;
    [SerializeField] private GameObject _laserTriplePrefab;
    [SerializeField] private GameObject _shield;
    [SerializeField] private GameObject[] _hurts;
    [SerializeField] private GameObject _mainCamera;
    [SerializeField] private AudioClip _laserClip;    
    private List<GameObject> _powerups;
    private AudioSource _audioSource;
    private CameraShake _cameraShake;
    private WaitForSeconds waitForSecs;
    private WaitForSeconds _seekingTimeWaitForSecs;
    private WaitForSeconds _restartThrustlerWaitForSecs;
    private SpriteRenderer _shieldSpriteRenderer;
    private float _shootRate = 0.5f;
    private float _coolDownRate = 3f;
    private float _canFire = -1f;
    private float _speedIncrease = 2f;
    private float _currentThrustler = 0;
    private int _score = 0;
    private int _hurtIndex = -1;
    private int _shieldStrength = 3;
    private int _thrustlerLimit = 15;
    private int _ammo = 0;
    private bool _isTripleShotEnabled = false;
    private bool _isSpeedBoostEnabled = false;
    private bool _isShieldEnabled = false;
    private bool _isSlowMovement = false;
    private bool _isSeekingTarget = false;
 

    #endregion

    #region UnityMethods
    void Start()
    { 
        transform.position = new Vector3(0, 0, 0);
        waitForSecs = new WaitForSeconds(_coolDownRate);
        _audioSource = gameObject.GetComponent<AudioSource>();
        _audioSource.clip = _laserClip;
        _shieldSpriteRenderer = _shield.GetComponent<SpriteRenderer>();
        _cameraShake = _mainCamera.GetComponent<CameraShake>();
        _seekingTimeWaitForSecs = new WaitForSeconds(5f);
        _restartThrustlerWaitForSecs = new WaitForSeconds(3f);
        _powerups = new List<GameObject>();
    }


    void Update()
    {


        _currentThrustler += Time.deltaTime;

        if (_currentThrustler <= _thrustlerLimit)
        {
            UIManager.Instance.UpdateThrustler(_currentThrustler);
            Move();
            BoundariesCalculation();
        }
        else
            StartCoroutine("restartThrustlerRoutine");

        if (Input.GetKeyDown(KeyCode.Space) && Time.time > _canFire && _ammo < _maxAmmo)
        {
            FireLaser();
        }

        if (Input.GetKey(KeyCode.C))
        {
            CollectPowerups();
        }
    }
    #endregion

    #region PrivateMethods
    private void Move()
    {
        Vector3 translatePosition = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        float currentSpeed = _speed;

        if (Input.GetKey(KeyCode.LeftShift) && !_isSlowMovement)
            currentSpeed = _speed * _speedIncrease;

        if (_isSpeedBoostEnabled)
        {
            transform.Translate(translatePosition * (currentSpeed * _speedIncrease) * Time.deltaTime);
        }
        else if (_isSlowMovement)
        {
            transform.Translate(translatePosition * (currentSpeed - 2f) * Time.deltaTime);
        }
        else
        {
            transform.Translate(translatePosition * currentSpeed * Time.deltaTime);
        }

    }

    private void BoundariesCalculation()
    {
        Vector3 screenLimit = transform.position;

        if (transform.position.y >= 5)
        {
            screenLimit.y = 5;
            transform.position = screenLimit;
        }
        else if (transform.position.y <= -3.8f)
        {
            screenLimit.y = -3.8f;
            transform.position = screenLimit;
        }

        if (transform.position.x >= 10f)
        {
            screenLimit.x = -10f;
            transform.position = screenLimit;
        }
        else if (transform.position.x <= -10f)
        {
            screenLimit.x = 10f;
            transform.position = screenLimit;
        }
    }

    private void FireLaser()
    {
        _canFire = Time.time + _shootRate;
        if (_isTripleShotEnabled)
        {
            var laser = Instantiate(_laserTriplePrefab, transform.position, Quaternion.identity);
        }
        else
        {
            var laser = Instantiate(_laserPrefab, new Vector3(transform.position.x, transform.position.y + 1.05f, 0), Quaternion.identity);
            laser.gameObject.GetComponent<Laser>().SetSeekingTarget(_isSeekingTarget);
        }

        _ammo++;
        UIManager.Instance.UpdateAmmo(_ammo, _maxAmmo);
        _audioSource.Play();
    }
    private void SetColorShield()
    {
        switch (_shieldStrength)
        {
            case 1:
                _shieldSpriteRenderer.color = Color.red;
                break;
            case 2:
                _shieldSpriteRenderer.color = Color.green;
                break;
        }
    }

    private void RandomHurtWing()
    {
        int initialHurt = Random.Range(0, 2);
        _hurtIndex = _hurtIndex == -1 ? initialHurt : _hurtIndex == 1 ? 0 : 1;
        _hurts[_hurtIndex].SetActive(true);
    }

    private void CollectPowerups()
    {
        _powerups = GameObject.FindGameObjectsWithTag("Powerup").ToList();

        if (_powerups.Count > 0)
        {
            foreach (var item in _powerups)
            {                
                item.transform.position = Vector3.MoveTowards(item.transform.position, transform.position, 3f * Time.deltaTime);
            }
        }
    }

    #endregion

    #region Coroutines
    IEnumerator DeactivateTripleShotRountine()
    {
        yield return waitForSecs;
        _isTripleShotEnabled = false;
    }

    IEnumerator DeactivateSpeedRountine()
    {
        yield return waitForSecs;
        _isSpeedBoostEnabled = false;
    }
    IEnumerator SetSeekingTimeRoutine()
    {
        yield return _seekingTimeWaitForSecs;
        _isSeekingTarget = false;
    }
    IEnumerator restartThrustlerRoutine()
    {
        yield return _restartThrustlerWaitForSecs;
        _currentThrustler = 0;
    }

    IEnumerator DeactivateSlowMovementRountine()
    {
        yield return waitForSecs;
        _isSlowMovement = false;
    }
    #endregion

    #region PublicMethods
    public void Damage()
    {
        if (_isShieldEnabled)
        {
            _shieldStrength--;
            SetColorShield();

            if (_shieldStrength == 0)
            {
                _isShieldEnabled = false;
                _shield.SetActive(false);
                _shieldStrength = 3;
                _shieldSpriteRenderer.color = Color.white;
            }
            return;
        }

        StartCoroutine(_cameraShake.Shake(0.15f, 0.1f));

        _lives--;
        RandomHurtWing();
        UIManager.Instance.UpdateLives(_lives);

        if (_lives == 0)
        {
            SpawnManager.Instance.StopSpawning();
            Destroy(this.gameObject);
        }
    }

    public void ActivateTripleShot()
    {
        _isTripleShotEnabled = true;
        StartCoroutine("DeactivateTripleShotRountine");
    }



    public void ActivateSpeed()
    {
        _isSpeedBoostEnabled = true;
        StartCoroutine("DeactivateSpeedRountine");
    }



    public void ActivateShield()
    {
        _shieldStrength = 3;
        _shieldSpriteRenderer.color = Color.white;
        _isShieldEnabled = true;
        _shield.SetActive(true);
    }

    public void ScorePoints(int points)
    {
        _score += points;
        UIManager.Instance.UpdateScore(_score);
    }

    public void RechargeAmmo()
    {
        _ammo = 0;
        UIManager.Instance.UpdateAmmo(_ammo, _maxAmmo);
    }

    public void RechargeHealth()
    {
        if (_lives < 3)
        {
            _lives++;
            UIManager.Instance.UpdateLives(_lives);
            _hurtIndex = _hurts[0].activeSelf ? 0 : 1;
            _hurts[_hurtIndex].SetActive(false);
        }
    }

    public void ActivateSeekingTarget()
    {
        _isSeekingTarget = true;
        StartCoroutine("SetSeekingTimeRoutine");
    }

    public void ActivateSlowMovement()
    {
        _isSlowMovement = true;
        StartCoroutine("DeactivateSlowMovementRountine");
    }
    #endregion

}
