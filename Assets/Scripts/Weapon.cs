﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    
    [SerializeField] protected float _speed = 10f;    
    [SerializeField] protected float _rotateSpeed = 1000f;
    protected Rigidbody2D _rigidBody;
    protected GameObject _player;

    protected virtual void Start()
    {
        _player = GameObject.Find("Player");
        _rigidBody = GetComponent<Rigidbody2D>();
        Transform parentLaser = transform.parent;

        if (parentLaser)
            Destroy(parentLaser.gameObject, 3f);
        else
            Destroy(this.gameObject, 3f);

    }
}
