﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField] protected GameObject _weaponShotPrefab;
    protected AudioSource _audioSource;
    protected Collider2D _collider2d;
    protected Animator _animator;
    protected Player _player;
    protected float _speed = 3f;
    protected float frequency = 5.0f;
    protected float _shootRate = 2f;
    protected float _canFire = -1f;
    protected bool _isDeath = false;


    protected virtual void Start()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();
        _audioSource = gameObject.GetComponent<AudioSource>();
        _collider2d = gameObject.GetComponent<Collider2D>();
        _animator = GetComponent<Animator>();
    }

    protected abstract void Move();
    protected abstract void Fire();
    protected abstract void DestroyEnemy();    
}
