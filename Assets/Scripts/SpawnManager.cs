﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    #region Properties
    [SerializeField] private GameObject _enemyBossPrefab;
    [SerializeField] private GameObject _enemyPrefab;
    [SerializeField] private GameObject _enemyContainer;
    [SerializeField] private GameObject _powerupContainer;    
    [SerializeField] private GameObject[] _powerupPrefabs;
    [SerializeField]private int _maxEnemiesWave = 4;
    private WaitForSeconds _powerupWaitForSecs;    
    private WaitForSeconds _healthPowerupWaitForSecs;
    private WaitForSeconds _ammoPowerupWaitForSecs;
    private bool _stopSpawing=true;
    private bool _setShield = false;        
    private int _waveNumber = 1;
    private int _enemyCount = 0;
    private static SpawnManager _instance;       
    public static SpawnManager Instance
    {
        get
        {
            if (_instance == null)
                Debug.LogError("UIManager is null");

            return _instance;
        }
    }

    #endregion

    #region UnityMethods
    private void Awake()
    {
        _instance = this;
    }

    private void Update()
    {
        RunWave();
    }

    void Start()
    {        
        _powerupWaitForSecs = new WaitForSeconds(10);
        _ammoPowerupWaitForSecs = new WaitForSeconds(7);
        _healthPowerupWaitForSecs = new WaitForSeconds(20);        
    }

    #endregion  
    

    #region Coroutines 

    IEnumerator SpawnPowerupRoutine()
    {
        while (!_stopSpawing)
        {            
            yield return _powerupWaitForSecs;
            int powerupType = Random.Range(0, 5);
            SpawnPowerup(powerupType);
        }
    }
    IEnumerator SpawnAmmoPowerupRoutine()
    {
        while (!_stopSpawing)
        {            
            yield return _ammoPowerupWaitForSecs;
            SpawnPowerup(5);
        }
    }
    IEnumerator SpawnHealthPowerupRoutine()
    {
        while (!_stopSpawing)
        {            
            yield return _healthPowerupWaitForSecs;
            SpawnPowerup(6);
        }
    }    

    #endregion

    #region PrivateMethods
    private void SpawnPowerup(int powerupType)
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-9.6f, 9.6f), 7, 0);
        var powerup = Instantiate(_powerupPrefabs[powerupType], spawnPosition, Quaternion.identity);
        powerup.transform.SetParent(_powerupContainer.transform);
    }

    private void RunWave()
    {
        if (!_stopSpawing)
        {
            _enemyCount = FindObjectsOfType<Enemy>().Length;

            if (_enemyCount == 0)
            {
                if (_waveNumber == _maxEnemiesWave)
                    Instantiate(_enemyBossPrefab);
                else
                {                    
                    SpawnEnemyWave(_waveNumber);
                    _waveNumber++;
                }
            }
        }
    }
    private void SpawnEnemyWave(int enemiesToSpawn)
    {
        for (int i = 0; i < enemiesToSpawn; i++)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-9f, 9f), 7, 0);

            var enemy = Instantiate(_enemyPrefab, spawnPosition, Quaternion.identity);
            enemy.transform.SetParent(_enemyContainer.transform);

            if (_setShield)
                enemy.GetComponent<EnemySpaceShip>().ActivateShield();

            _setShield = _setShield ? false : true;
        }
    }

    #endregion

    #region PublicMethods

    public void StartSpawning()
    {
        _stopSpawing = false;        
        StartCoroutine("SpawnPowerupRoutine");
        StartCoroutine("SpawnAmmoPowerupRoutine");
        StartCoroutine("SpawnHealthPowerupRoutine");
    }

    public void StopSpawning()
    {
        _stopSpawing = true;
    }
    #endregion
}
