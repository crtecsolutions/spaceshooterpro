﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBoss : Enemy
{

    [SerializeField] private int _lives=10;
    protected override void Start()
    {
        base.Start();
        _shootRate = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isDeath)
        {
            if (IsInCenterPosition())
                Fire();
            else
                Move();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {        
        if (other.tag == "Laser")
        {
            Destroy(other.gameObject);
            DestroyEnemy();
        }
    }


    protected override void DestroyEnemy()
    {
        _lives--;
        if (_lives == 0)
           {
            if (_animator)
               _animator.SetTrigger("OnEnemyDeath");

            _audioSource.Play();
            _isDeath = true;
            Destroy(this.gameObject,2.6f);
            SpawnManager.Instance.StopSpawning();
            UIManager.Instance.SetGameWinner();
        }
    }

    protected override void Fire()
    {
        if (Time.time > _canFire)
        {
            
            _canFire = Time.time + _shootRate;

            if (_player)
            {
                _animator.SetTrigger("Attack");
                GameObject enemyLaser = Instantiate(_weaponShotPrefab, transform.position, Quaternion.identity);
                EnemyLaser[] lasers = enemyLaser.GetComponentsInChildren<EnemyLaser>();

                foreach (EnemyLaser laser in lasers)
                {
                    laser.SetEnemyLaser(3);
                }
            }
            else
                _animator.SetBool("Idle", true);
        }
    }

    protected override void Move()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
    }

    private bool IsInCenterPosition()
    {
        if (transform.position.y >= 3f)
            return false;

        return true;
    }



}
