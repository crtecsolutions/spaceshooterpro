﻿using System.Collections;
using UnityEngine;

public class Laser : Weapon
{
    #region Properties      
    [SerializeField] protected Transform _target;
    private bool _isSeekingTarget = false;        
    #endregion

    #region UnityMethods
    protected override void Start()
    {
        base.Start();
    }

    void FixedUpdate()
    {
        if (_isSeekingTarget)
        {
            SeekingTarget();
        }
        else
        {
            transform.Translate(Vector3.up * _speed * Time.deltaTime);
        }
    }
    
    #endregion

    #region PrivateMethods

    private void SeekingTarget()
    {       
        Vector2 direction = (Vector2)_target.position - _rigidBody.position;
        direction.Normalize();
        float rotateAmount = Vector3.Cross(direction, transform.up).z;
        _rigidBody.angularVelocity = -rotateAmount * _rotateSpeed;
        _rigidBody.velocity = transform.up * _speed;
    }
    #endregion

    #region PublicMethods

    public void SetSeekingTarget(bool isSeeking)
    {
        _isSeekingTarget = isSeeking;
    }

    #endregion
}
