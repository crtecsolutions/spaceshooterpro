﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    #region Properties
    [SerializeField] private float _speed = 3f;
    [SerializeField] private int _powerupId=0; //0= triple shot, 1= speed boost, 3 = shield
    [SerializeField] private AudioClip _audioClip;
    #endregion

    #region UnityMethods
    void Update()
    {

        transform.Translate(Vector3.down * _speed * Time.deltaTime);
        CalculateBoundaries();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            AudioSource.PlayClipAtPoint(_audioClip, transform.position);

            if (player)
                ActivatePowerup(player);

            Destroy(this.gameObject);
        }
    }
    #endregion

    #region PrivateMethods
    private void CalculateBoundaries()
    {
        if (transform.position.y < -5.8)
            Destroy(this.gameObject);
    }
    
    private void ActivatePowerup(Player player)
    {
        switch (_powerupId)
        {
            case 0:
                player.ActivateTripleShot();
                break;
            case 1:
                player.ActivateSpeed();
                break;
            case 2:
                player.ActivateShield();
                break;
            case 3:
                player.ActivateSeekingTarget();
                break;
            case 4:
                player.ActivateSlowMovement();
                break;
            case 5:
                player.RechargeAmmo();
                break;
            case 6:
                player.RechargeHealth();
                break;
        }
    }
    #endregion
}
