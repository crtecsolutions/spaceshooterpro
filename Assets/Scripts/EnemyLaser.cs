﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLaser : Weapon
{
    private int _enemyLaserType = -1;    
    protected override void Start()
    {
        base.Start();        
    }

    void FixedUpdate()
    {
        if (_player)
        {

            if (_enemyLaserType == 1)
            {
                transform.Translate(Vector3.down * _speed * Time.deltaTime);
            }
            else if (_enemyLaserType == 3)
            {
                Vector3 lookDirection = (_player.transform.position - transform.position).normalized;
                this._rigidBody.AddForce(lookDirection * _speed);
            }
            else
            {
                transform.Translate(Vector3.up * _speed * Time.deltaTime);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player)
                player.Damage();
        }
        else if (other.tag == "Powerup")
        {
            Destroy(other.gameObject);
        }

    }

    public void SetEnemyLaser(int type)
    {
        _enemyLaserType = type;
    }
}
